<?php /* Template Name: home */ ?>
<?php get_header() ?>
<div id="home" class="home">
	<section id="sec1">
	
		<div class="center row text-center">
			<img class="logoh" src="<?=urltheme()?>/img/logosubaru.png">
			<br class="show-for-large"><br class="show-for-large">
			<?php while( have_posts() ):?>
				
				<?php the_post() ?>
				<div class="clearfix">
					<?php the_title('<h1 class="tituloh columns large-8 large-offset-2">','</h1>')?>
				</div>
			<div class="columns large-4 large-offset-8 medium-6 medium-offset-6 msg-sec1">
				<div class="descripcion text-right">
					<?php the_content() ?>
					<?php 
						$seccion2 = get_post_meta( get_the_ID(), 'seccion2', true);
						$seccion3 = get_post_meta( get_the_ID(), 'seccion3', true);
						$seccion4 = get_post_meta( get_the_ID(), 'seccion4', true);
						$seccion5 = get_post_meta( get_the_ID(), 'seccion5', true);
					?>
				</div>	
				<img class="hleft" src="<?=urltheme()?>/img/finbe.png">				
				<img class="hright" src="<?=urltheme()?>/img/financierabepensa.png">				
			</div>
			<?php endwhile;?>			
		</div>
	</section>
	<section id="seccion2">
	<div class="diagonal"></div>
		<div id="content-seccion2" class="car">
			<div class="row center text-center">
				<?=$seccion2?>				
			</div>
		</div>
	</section>
	<section id="tres">
		<div id="elcar3" class="center text-center">
			<?=nl2br($seccion3)?>			
		</div>
	</section>
	<section>
		<div class="center row">
			<h1 class="text-center contacto">contáctanos</h1>
			<br>
			<div class="columns large-4 medium-6 small-12">
				<?=nl2br($seccion4)?>				
			</div>
			<div class="columns large-8 medium-6 small-12" style="padding:0px;">
				<br class="show-for-small">
				<?=do_shortcode('[contact-form-7 id="4" title="Contacto"]')?>
			</div>
			
		</div>
	</section>
	<section>
		<div class="row">
			<div class="columns large-4 medium-4 small-12 foterlogo">
				<img src="<?=urltheme()?>/img/logofoter.png">
				<br class="show-for-small-only">
				<br class="show-for-small-only">
			</div>
			<div class="columns large-8 medium-8 small-12">
				<p><?=nl2br($seccion5)?></p>				
			</div>
		</div>
	</section>
</div>
<?php get_footer() ?>