<?php 
function generic_editor( $elid, $field)
{
    $seccion = get_post_meta( $elid , $field, true );
    $seccion = (!empty($seccion)) ? $seccion : ''; 
    wp_editor( $seccion, $field, array('textarea_name'=>$field)); 
}
function cyb_meta_box_seccion2(){
    global $post;
    generic_editor($post->ID,'seccion2');
}
function cyb_meta_box_seccion3(){
    global $post;
    generic_editor($post->ID,'seccion3');
}
function cyb_meta_box_seccion4(){
    global $post;
    generic_editor($post->ID,'seccion4');
}

function cyb_meta_box_seccion5(){
    global $post;
    generic_editor($post->ID,'seccion5');
}

add_action('add_meta_boxes', 'cyb_meta_boxes');
function cyb_meta_boxes(){
	add_meta_box( 
        'cyb-meta-box_seccion2'
        ,__('Seccion2')
        ,'cyb_meta_box_seccion2'
        ,'page'
    );
    add_meta_box( 
        'cyb-meta-box_seccion3'
        ,__('Seccion3')
        ,'cyb_meta_box_seccion3'
        ,'page'
    );
    add_meta_box( 
        'cyb-meta-box_seccion4'
        ,__('Seccion4')
        ,'cyb_meta_box_seccion4'
        ,'page'
    );
    add_meta_box( 
        'cyb-meta-box_seccion5'
        ,__('Seccion5')
        ,'cyb_meta_box_seccion5'
        ,'page'
    );
}
add_action( 'save_post', 'dynamic_save_postdata' );

function dynamic_save_postdata() {

    global $post;
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
    {   return; }
	
	if (isset($_POST['seccion2'])) {
		update_post_meta($post->ID,'seccion2',$_POST['seccion2']);    
	}
	if (isset($_POST['seccion3'])) {
		update_post_meta($post->ID,'seccion3',$_POST['seccion3']);    
	}
	if (isset($_POST['seccion4'])) {
		update_post_meta($post->ID,'seccion4',$_POST['seccion4']);    
	}
	if (isset($_POST['seccion5'])) {
		update_post_meta($post->ID,'seccion5',$_POST['seccion5']);    
	}

}